# I just wanna make sure that you won't install the older version than mine!
Vagrant.require_version ">= 2.2.14", "< 3.0.0"

# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  # explicitly define a VM, instead of 'default'
  config.vm.define "bionic"

  # add fstab entries for synced folders, automatically be mounted on machine reboot
  config.vm.allow_fstab_modification = true

  # allow writing to /etc/hosts
  config.vm.allow_hosts_modification = true
  config.vm.hostname = "bionic-beaver"

  # wait for the guest machine to boot (in seconds)
  config.vm.boot_timeout = 120

  # communicator type to use to connect to the guest box
  config.vm.communicator = "ssh"

  # guest OS that will be running within this machine
  config.vm.guest = :linux

  # still load the Vagrantfile inside a box, if present
  config.vm.ignore_box_vagrantfile = false

  # message shown after vagrant up
  config.vm.post_up_message = "Hi bro, Hino here, happy learning!"

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "hashicorp/bionic64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network :forwarded_port, guest: 80, host: 9999

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder ".", "/vagrant/", disabled: true
  # vboxsf = vbox share folder
  config.vm.synced_folder "./html", "/vagrant/html", id: "html", type: "virtualbox", create: false

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    # but since we connect via ssh, we don't have the login credentials LOL
    # vb.gui = true

    # VM name, default: DIRECTORY_default_TIMESTAMP
    vb.name = "bionic-vm"

    # Customize the amount of memory on the VM
    vb.memory = "1024"
    vb.cpus = "2"

    # not necesary, skip to reduce boot time
    vb.check_guest_additions = false

    # using vboxmanage utility CLI
    # vb.customize ["modifyvm", :id, "--name", "another-name"]
    vb.customize ["modifyvm", :id, "--description", "Gotta catch 'em all!"]
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "80"]
    vb.customize ["modifyvm", :id, "--autostart-enabled", "off"]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  config.vm.provision :shell, path: "bootstrap.sh",
    keep_color: false, privileged: true, sensitive: false,
    upload_path: "/tmp/vagrant-shell"

  # replicate gitconfig into VM
  config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"
  config.vm.provision "file", source: "~/.curlrc", destination: ".curlrc"

end
