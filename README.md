# Prerequisites
- virtualbox 6.0+ (https://www.virtualbox.org/wiki/Linux_Downloads)

  **tips:** with ubuntu 20.04, you can install it from apt package upstream

- vagrant 2.2+ (https://www.vagrantup.com/downloads)

  **tips:** with ubuntu 20.04, you can install it from apt package upstream as well

- and a beautiful soul :dolphin: :tiger: :snail:

# The simple parts

- from the current dir (with Vagrantfile):
  ```bash
  vagrant up
  ```

- enjoy port-forwarding, we have an nginx server listening on port `80` inside guest machine, accessible via port `9999` on host machine:
  ```bash
  curl -sX GET localhost:9999
  ```

# Useful links

- Bash autocompletion for vagrant CLI (https://www.vagrantup.com/docs/cli)

# Play with docker provider

- We use an SSH server from guys at linuxserver.io (https://docs.linuxserver.io/images/docker-openssh-server)
  ```bash
  cd docker-provider/
  vagrant ssh-config
  ```

- Try those docker-related commands from vagrant:
  ```bash
  vagrant docker-logs
  vagrant docker-exec -it -- bash
  ```

- You can even ssh into it, despite being discouraged (http://jpetazzo.github.io/2014/06/23/docker-ssh-considered-evil/) :sheep:
  ```bash
  vagrant ssh
  ```
  **Disclaimer:** This ssh server doesn't support SCP, thus the file and shell provisioners won't work, let me take another look later.

- Another way to ssh into using ssh command:
  ```bash
  ssh vagrant@127.0.0.1 -p 2000 -i privkey/vagabond
  ```
